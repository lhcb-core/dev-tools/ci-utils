# Utilities for CI jobs

This project is a collection of utilities to simplify the development of LHCb Gitlab-CI configurations and to keep them up to date throughout the projects.
